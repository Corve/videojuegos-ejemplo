from multiprocessing import context
from django.shortcuts import render


def hola_mundo(request):
    return render(request, 'mensaje.html')

def mensaje(request):
    context = {
        'nombre' : 'Alex Mauricio',
        'edad' : 41,
        'materias': [
            'Frameworks',
            'Deployment',
            'Administración de proyectos de software',
            'SO Linux',
            'Pruebas y mantenimiento de software',
        ],
        'calificaciones':{
            'frameworks': 10,
            'algebra': 8,
            'requerimientos': 9,
            'linux': 10,
        }
    }
    return render(request, 'mensaje_variables.html', context)

def mensaje_variables(request, edad, nombre):
    # print(request.GET)
    # print(request.GET.get('edad', None))
    context = {
        'edad': edad,
        'nombre': nombre
    }
    return render(request, 'mensaje_variables.html', context)
    